// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "practical_work_17/practical_work_17GameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodepractical_work_17GameModeBase() {}
// Cross Module References
	PRACTICAL_WORK_17_API UClass* Z_Construct_UClass_Apractical_work_17GameModeBase_NoRegister();
	PRACTICAL_WORK_17_API UClass* Z_Construct_UClass_Apractical_work_17GameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_practical_work_17();
// End Cross Module References
	void Apractical_work_17GameModeBase::StaticRegisterNativesApractical_work_17GameModeBase()
	{
	}
	UClass* Z_Construct_UClass_Apractical_work_17GameModeBase_NoRegister()
	{
		return Apractical_work_17GameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_Apractical_work_17GameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_Apractical_work_17GameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_practical_work_17,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_Apractical_work_17GameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "practical_work_17GameModeBase.h" },
		{ "ModuleRelativePath", "practical_work_17GameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_Apractical_work_17GameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<Apractical_work_17GameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_Apractical_work_17GameModeBase_Statics::ClassParams = {
		&Apractical_work_17GameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_Apractical_work_17GameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_Apractical_work_17GameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_Apractical_work_17GameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_Apractical_work_17GameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(Apractical_work_17GameModeBase, 436608926);
	template<> PRACTICAL_WORK_17_API UClass* StaticClass<Apractical_work_17GameModeBase>()
	{
		return Apractical_work_17GameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_Apractical_work_17GameModeBase(Z_Construct_UClass_Apractical_work_17GameModeBase, &Apractical_work_17GameModeBase::StaticClass, TEXT("/Script/practical_work_17"), TEXT("Apractical_work_17GameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(Apractical_work_17GameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
